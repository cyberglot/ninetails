var layers = []

var update = function (fn) {
  return function(){
    layers = fn.apply(null, [layers].concat([].slice.call(arguments)))
  }
}

var getBound = function (type) {
  return function(elm){
    elm.component[elm[type]]()
    return elm
  }
}

var bind   = getBound('bind')
var unbind = getBound('unbind')

var push = update(function(layers, component, add, remove){
  component[add]()
  return layers
    .map(unbind)
    .concat([{
        component: component
      , bind: add
      , unbind: remove
    }])
})

var pop = update(function(layers){
  var length = layers.length
  return layers
    .map(function(elm, idx) {
      switch (idx){
        case (length - 2) : return bind(elm)
        default           : return unbind(elm)
      }
    })
    .slice(0, length - 1)
})

var clear = update(function(layers){
  return layers
    .map(unbind)
    .slice(0, 0)
})

var size = function() {
  return layers.length
}


module.exports = {
    push: push
  , pop: pop
  , clear: clear
  , size: size
}
