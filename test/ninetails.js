import EventEmitter from 'eventemitter3'
import {expect} from 'chai'

import * as LayerManager from '../src/ninetails.js'

const Counter = function(){
  this.value = 0
  this.ee = new EventEmitter()
}

Counter.prototype.startListening = function() {
  this.ee.on('inc', this.inc, this)
}

Counter.prototype.stopListening = function() {
  this.ee.removeListener('inc', this.inc, this)
}

Counter.prototype.inc = function() {
  this.value = this.value + 1
}

const sum = (y, x) => x.value + y

describe('LayerManager', () => {
  var counters, length

  beforeEach(() => {
    LayerManager.clear()
    counters = []
    counters.push(new Counter())
    counters.push(new Counter())
    counters.push(new Counter())
    counters.push(new Counter())

    length = counters.length
  })

  it('should add multiple layers', () => {
    counters.forEach((c) => LayerManager.push(c, 'startListening', 'stopListening'))
    counters.forEach((c) => c.ee.emit('inc'))

    expect(counters.reduce(sum, 0)).to.equal(1)
  })

  it('should remove multiple layers', () => {
    counters.forEach((c) => LayerManager.push(c, 'startListening', 'stopListening'))
    counters.forEach((c) => c.ee.emit('inc'))
    LayerManager.pop()

    expect(LayerManager.size()).to.equal(3)
    expect(counters.slice(0, length - 1).reduce(sum, 0)).to.equal(0)
  })

  it('should clear the layers', () => {
    counters.forEach((c) => LayerManager.push(c, 'startListening', 'stopListening'))
    LayerManager.clear()

    expect(LayerManager.size()).to.equal(0)
  })
})
